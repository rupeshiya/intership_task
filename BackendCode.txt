/////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////// FOR GENERATING TOKEN /////////////////////////////////////////

var JwtStrategy = require('passport-jwt').Strategy;
var ExtractJwt = require('passport-jwt').ExtractJwt;
const User = require('../models/users');
require('dotenv').config();

module.exports = function(passport){
  var opts = {} // options
      opts.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken();
      opts.secretOrKey = process.env.secret;
      //payload contains the userinfo
      passport.use(new JwtStrategy(opts, function(jwt_payload, done) {
      console.log('jwt_payload info',jwt_payload);
          User.getUserById({id: jwt_payload.data._id}, function(err, user) {
              if (err) {
                  return done(err, false);
              }
              if (user) {
                  return done(null, user);
              } else {
                  return done(null, false);
                  // or you could create a new account
              }
          });
}));
}

//////////////////////////////////////////////////////////////////////////////////////////////////////

/////////////////////////////// FOR CREATING AND COMPARE PASSWORD ////////////////////////////////////


const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const bcrypt = require('bcryptjs');

var userSchema = new  Schema({
  username:{
    type: String,
    required: true,
    unique: false
  },
  email:{
    type: String,
    required: true,
    unique: false
  },
  password:{
    type: String,
    required: true,
    // minlength: 6
  },
  name:{
    type: String,
    require: true,
    // minlength: 3,
    unique: false
  }
})

module.exports = mongoose.model('Users',userSchema);
// created for using this in our function 
const Users = mongoose.model('Users',userSchema);

// function to find user by id
module.exports.getUserById = function(id,callback){
  Users.findById(id,callback);
}

// function to find user by username
module.exports.getUserByUsername = function(username,callback){
  const query = {username: username}
  Users.findOne(query,callback);
}

// function to add user
module.exports.addUser = function(newUser, callback){
  bcrypt.genSalt(10, (err,salt) => {
    bcrypt.hash(newUser.password, salt, (err, hash) => {
      if(err){
        console.log(err);
      };
      // after hash changed plain password into hashed password
      newUser.password = hash;
      newUser.save(callback);
    });
  })
}
 
// function to compare passwordd
module.exports.comparePassword = function(candidatePassword, hash,callback){
  console.log('candidatePassword-', candidatePassword);
  console.log('hashPassword-',hash);
  bcrypt.compare(candidatePassword,hash,(err,isMatch) =>{
    if(err){
      console.log(err);
    };
   callback(null,isMatch);
  })
}

//////////////////////////////////////////////////////////////////////////////////////////////////////

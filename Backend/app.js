const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const cors = require('cors');
const morgan = require('morgan');
const session = require('express-session');
const cookieParser = require('cookie-parser');
const passport = require('passport');
const dotenv = require('dotenv');
dotenv.config();

const port = process.env.PORT;

const app = express();
// middleware
// parse application/x-www-form-urlencoded
app.use(cors());
app.use(morgan('combined'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
// session
app.use(cookieParser());
app.use(session({
    secret: process.env.secret,
    resave: true,
    saveUninitialized: false,
    cookie : {
        expires: false,
    }
}));

//Passport
app.use(passport.initialize());
app.use(passport.session());
// calling passport strategy function
require('./config/passport')(passport);

// connecting mongoose
mongoose.connect(process.env.mongoURI,{useNewUrlParser: true})
  .then(()=>{
    console.log('mongo connected !!');
  })
  .catch((err)=>{
    console.log('error in connecting mongo-',err);
  });


// routes
app.get('/test',(req,res)=>{
  res.status(200).json({success: true, msg: 'test works !!'});
});
const users = require('./routes/users');
app.use('/users',users);


// running port
app.listen(port,()=>{
  console.log(`server is listening on ${port}`);
});
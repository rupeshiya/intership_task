## Backend folder- [See Code](https://bitbucket.org/rupeshiya/intership_task/src/master/Backend/) 
  * Contains code for -
  * Backend (Server side)
  * Database

## Frontend Folder- [See Code](https://bitbucket.org/rupeshiya/intership_task/src/frontend/)
  * Contains code for-
  * Frontend 

## Features 
  * Login
  * Register
  * Restricted Routes
  * Dashboard
  
## Tech stacks used -
  * HTML
  * CSS
  * JS
  * NODE
  * MONGODB
  * ANGULAR
  * EXPRESS

### made with :heart: by [Rupeshiya](https://github.com/Rupeshiya)
